import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../User.interface';

@Injectable({
  providedIn: 'root'
})
export class UsersDbServiceService {

  private apiUrl = 'https://jsonplaceholder.typicode.com';
  constructor(private http: HttpClient) { }

  GetUsers(): Observable<any> {
    return this.http.get(`${this.apiUrl}/users`);
  }

  addUser(newUser:any): Observable<any> {  
    // Ajouter le nouvel utilisateur à la liste des utilisateurs stockés (localStorage)
    let storedUsers = this.getStoredUsers();
    storedUsers.push(newUser);
    console.log('newUser',newUser)
    localStorage.setItem('storedUsers', JSON.stringify(storedUsers));
    return of('newUser');
  }

  private generateUniqueId(): number {
    // Implémentez ici la logique pour générer un ID unique (par exemple, en utilisant un compteur ou une méthode unique)
    // Pour cet exemple, utilisons un timestamp pour un ID simple
    return new Date().getTime();
  }

  private getStoredUsers(): User[] {
    const storedUsers = localStorage.getItem('storedUsers');
    return storedUsers ? JSON.parse(storedUsers) : [];
  }
}
