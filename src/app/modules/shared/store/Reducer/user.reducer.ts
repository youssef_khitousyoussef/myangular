// user.reducer.ts

import { createReducer, on } from '@ngrx/store';
import { User } from '../../User.interface';
import { addUser } from '../action/user.action';

// Interface pour l'état de l'utilisateur
export interface UserState {
  users: User[];
}

// État initial
export const initialState: UserState = {
  users: []
};

// Reducer pour les utilisateurs
export const userReducer = createReducer(
  initialState,
  on(addUser, (state, { user }) => ({
    ...state,
    users: [...state.users, user]
  }))
);
