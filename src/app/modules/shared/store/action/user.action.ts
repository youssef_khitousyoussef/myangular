// user.state.ts

import { createAction, props } from '@ngrx/store';

// Interface pour l'utilisateur
export interface User {
  id: number;
  name: string;
  username: string;
}

// Actions pour la gestion des utilisateurs
export const addUser = createAction(
  '[User] Add User',
  props<{ user: User }>()
);

export const AddUserActionSuccess = createAction(
    '[User ADD]  SUCCESS',
    props<{ user: any }>()
  );
  export const ddUserActionFailed = createAction(
    '[User ADD]  FAILURE',
    props<{ error: string }>()
  );