// user.effects.ts

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { mergeMap, map } from 'rxjs/operators';
import { UsersDbServiceService } from '../../services/users-db-service.service'; // Assurez-vous d'importer le service correctement
import * as UserActions from '../action/user.action';

@Injectable()
export class UserEffects {

    addUser$ = createEffect(() =>
        this.actions$.pipe(
          ofType(UserActions.addUser),
          mergeMap(action =>
            this.userService.addUser(action.user).pipe(
              map(newUser => UserActions.AddUserActionSuccess({ user: newUser })),
              // Gérer les erreurs éventuelles avec catchError()
            )
          )
        )
      );

  constructor(
    private actions$: Actions,
    private userService: UsersDbServiceService
  ) {}
}
