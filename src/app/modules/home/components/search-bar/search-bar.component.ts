import { Component, ElementRef, ViewChild, inject } from '@angular/core';
import { BehaviorSubject, Observable, map, switchMap } from 'rxjs';
import { User } from 'src/app/modules/shared/User.interface';
import { UsersDbServiceService } from 'src/app/modules/shared/services/users-db-service.service';
import { Store } from '@ngrx/store';
import { addUser } from 'src/app/modules/shared/store/action/user.action';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css'],
})
export class SearchBarComponent {

  getUsers$ = this.userService.GetUsers(); // Exemple d'appel à un service pour obtenir les utilisateurs
  selectedUserId: number | null = null;
  filterValue: string = '';
  filterCriteria: string = '';
  private filterSubject = new BehaviorSubject<string>('');
  isAddUserModalOpen: boolean = false;
  isUserDetailModalOpen: boolean = false;
  newUser: { name: string, username: string } = { name: '', username: '' };
  selectedUser: User | null = null; // Pour stocker l'utilisateur sélectionné

  filteredUsers$!: Observable<User[]>;

  constructor(private userService: UsersDbServiceService,private store: Store<{ users: User[] }>) { }

  ngOnInit() {
    this.filteredUsers$ = this.filterSubject.asObservable().pipe(
      map(criteria => criteria.toLowerCase()),
      switchMap(criteria => {
        return this.userService.GetUsers().pipe(
          map(users => users.filter((user: User) => {
            if (this.filterCriteria === 'id') {
              return user.id.toString().includes(criteria);
            } else if (this.filterCriteria === 'name') {
              return user.name.toLowerCase().includes(criteria);
            } else if (this.filterCriteria === 'username') {
              return user.username.toLowerCase().includes(criteria);
            } else {
              return true;
            }
          }))
        );
      })
    );
  }

  likeUser(userId: number): void {
    let likedUsers = this.getLikedUsers();
    if (likedUsers.includes(userId)) {
      likedUsers = likedUsers.filter(id => id !== userId);
    } else {
      likedUsers.push(userId);
    }
    localStorage.setItem('likedUsers', JSON.stringify(likedUsers));
  }

  isLiked(userId: number): boolean {
    return this.getLikedUsers().includes(userId);
  }

  addUserToLocalStorage(user: User): void {
    let storedUsers = this.getStoredUsers();
    if (!storedUsers.find(storedUser => storedUser.id === user.id)) {
      storedUsers.push(user);
      localStorage.setItem('storedUsers', JSON.stringify(storedUsers));
    }
  }

  selectUser(userId: number): void {
    this.selectedUserId = this.selectedUserId === userId ? null : userId;
  }

  isSelected(userId: number): boolean {
    return this.selectedUserId === userId;
  }

  filterBy(criteria: string): void {
    this.filterCriteria = criteria;
    this.filterSubject.next(this.filterValue);
  }

  openAddUserModal(): void {
    this.isAddUserModalOpen = true;
  }

  closeAddUserModal(): void {
    this.isAddUserModalOpen = false;
    // Réinitialiser les champs du formulaire
    this.newUser = { name: '', username: '' };
  }


  submitNewUser(Name:string,UserName:string): void {
    // Vérifier que les champs requis sont remplis
    if (Name && UserName) {
      // Générer un ID unique pour le nouvel utilisateur (à implémenter selon vos besoins)
      const newUserId = this.generateUniqueId();
  
      // Créer un nouvel utilisateur avec les données du formulaire
      const newUser: User = {
        id: newUserId,
        name: Name,
        username: UserName
      };
  

      //dispatch
      this.store.dispatch(addUser({ user: newUser }));
  
      // Fermer le modèle d'ajout d'utilisateur après l'ajout
      this.closeAddUserModal();
  
      // Réinitialiser les champs du formulaire
      this.newUser = { name: '', username: '' };
  
      // Facultatif : afficher un message de confirmation ou effectuer d'autres actions après l'ajout
      console.log('Nouvel utilisateur ajouté avec succès:', newUser);
    } else {
      console.error('Veuillez remplir tous les champs du formulaire.');
      // Facultatif : gérer le cas où les champs requis ne sont pas remplis
    }
  }
  
  openUserDetailModal(user: any): void {
    this.selectedUser = user;
    this.isUserDetailModalOpen = true;
  }

  closeUserDetailModal(): void {
    this.isUserDetailModalOpen = false;
    // Réinitialiser les détails de l'utilisateur sélectionné si nécessaire
    this.selectedUser = null;
  }

  private generateUniqueId(): number {
    // Implémentez ici la logique pour générer un ID unique (par exemple, en utilisant un compteur ou une méthode unique)
    // Pour cet exemple, utilisons un timestamp pour un ID simple
    return new Date().getTime();
  }


  private getLikedUsers(): number[] {
    const likedUsers = localStorage.getItem('likedUsers');
    return likedUsers ? JSON.parse(likedUsers) : [];
  }

  private getStoredUsers(): User[] {
    const storedUsers = localStorage.getItem('storedUsers');
    return storedUsers ? JSON.parse(storedUsers) : [];
  }


}
