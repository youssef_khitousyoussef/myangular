import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchBarComponent } from './modules/home/components/search-bar/search-bar.component';
import {  HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { userReducer } from './modules/shared/store/Reducer/user.reducer';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from './modules/shared/store/Effect/user.effect';


@NgModule({
  declarations: [
    AppComponent,
    SearchBarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
     // Autres imports...
     StoreModule.forRoot({ users: userReducer }),
     EffectsModule.forRoot([UserEffects]) 

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
